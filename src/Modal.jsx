import React, { Component } from 'react'

export default class Modal extends Component {
  render() {
    let { detail } = this.props;
    return (
      <div className="modal fade" id='modalProduct'>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{detail.name}</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
            </div>
            <div className="modal-body">
              <img src={detail.image} className='img-fluid' style={{
                border: '2px solid black',
                borderRadius: '10px'
              }} alt={detail.image} />
              <div className='content mt-4 p-3' style={{
                border: '2px solid black',
                borderRadius: '10px'
              }}>
                <p className='text-success'>{detail.price} $</p>
                <p className='text-warning'>Mô tả:<br /><span>+ {detail.description}</span></p>
                <p>+ {detail.shortDescription}</p>
                <h5 className='text-primary'>Số lượng: {detail.quantity}</h5>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div >

    )
  }
}
