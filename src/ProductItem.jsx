import React, { Component } from 'react'

export default class ProductItem extends Component {

  render() {
    let { product } = this.props
    return (
      <div className='border border-2 rounded-3 p-3'>
        <img src={product.image} className='img-fluid' alt={product.image} />
        <div className='content'>
          <h5 className='text-danger'>{product.name}</h5>
          <p className='text-success'>{product.price} $</p>
          <button className='btn btn-dark' data-bs-toggle="modal" data-bs-target="#modalProduct" onClick={()=>{this.props.viewDetail(product)}}>Add to cart
            <i className="fa fa-cart-plus ms"></i>
          </button>
        </div>
      </div>
    )
  }
}
