import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class ProductList extends Component {

    renderProduct = () =>{
        return this.props.product.map((item,index)=>{
            return <div className='col-4' key={index}>
                <ProductItem product={item} viewDetail={this.props.viewDetail}/>
            </div>
        })
    }

    render() {
        return (
            <div className="row g-4">
                {this.renderProduct()}
            </div>
        )
    }
}
